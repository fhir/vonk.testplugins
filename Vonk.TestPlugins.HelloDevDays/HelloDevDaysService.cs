﻿using Hl7.Fhir.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using Vonk.Core.Context;
using Vonk.Fhir.R4;

namespace Vonk.TestPlugins.HelloDevDays
{
    public class HelloDevDaysService
    {
        private readonly ILogger<HelloDevDaysService> _logger;

        public HelloDevDaysService(ILogger<HelloDevDaysService> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// This method will return a Person called "Hello DevDays!" on a read request to /Person/Hello
        /// </summary>
        /// <param name="context"></param>
        public void SayHello(IVonkContext context)
        {
            if (context.Arguments.ResourceId() == "Hello")
            {
                _logger.LogTrace("Client asked for a hello!");
                context.Response.Payload =
                    new Person()
                    {
                        Id = "Hello",
                        Meta = new Meta { VersionId = "v1" },
                        Name = new List<HumanName> { new HumanName { Given = new[] { "Hello" }, Family = "DevDays!" } }
                    }
                    .ToIResource();
                context.Arguments.ResourceTypeArguments().Handled();
                context.Arguments.ResourceIdArgument().Handled();
                context.Response.HttpResult = StatusCodes.Status200OK;
            }
        }
    }
}
