﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Vonk.Core.Common;
using Vonk.Core.Context;
using Vonk.Core.Pluggability;

namespace Vonk.TestPlugins.HelloDevDays
{
    [VonkConfiguration(order: 4408)] //Normal read is 4410, so this should come before it. Otherwise the normal ReadService will always handle the request.
    public static class HelloDevDaysConfiguration
    {
        public static IServiceCollection AddHelloDevDaysServices(this IServiceCollection services)
        {
            services.TryAddSingleton<HelloDevDaysService>();
            return services;
        }

        public static IApplicationBuilder UseHelloDevDays(this IApplicationBuilder app)
        {
            return app
                .OnInteraction(VonkInteraction.instance_read)
                .AndResourceTypes(new[] { "Person" })
                .AndInformationModel(VonkConstants.Model.FhirR4)
                .HandleWith<HelloDevDaysService>
                (
                    (svc, ctx) => svc.SayHello(ctx)
                );
        }
    }
}
