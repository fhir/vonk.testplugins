﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Vonk.Core.Common;
using Vonk.Core.Context;
using Vonk.Core.Pluggability;

namespace Vonk.TestPlugins.HelloDevDays
{
    [VonkConfiguration(order: 4408)] //Normal read is 4410, so this should come before it. Otherwise the normal ReadService will always handle the request.
    public static class HelloDevDaysPostHandlerConfiguration
    {
        public static IServiceCollection AddHelloDevDaysPostHandlerServices(this IServiceCollection services)
        {
            services.TryAddSingleton<HelloDevDaysService>();
            return services;
        }

        public static IApplicationBuilder UseHelloDevDaysPostHandler(this IApplicationBuilder app)
        {
            return app
                .OnInteraction(VonkInteraction.instance_read)
                .AndInformationModel(VonkConstants.Model.FhirR4)
                .AndResourceTypes(new[] { "Person" })
                .AndStatusCodes(new[] { StatusCodes.Status404NotFound, StatusCodes.Status410Gone })
                .PostHandleWith<HelloDevDaysService>
                (
                    (svc, ctx) => svc.SayHello(ctx)
                );
        }
    }
}
