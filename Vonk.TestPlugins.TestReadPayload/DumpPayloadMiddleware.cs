﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Vonk.Core.Pluggability;

namespace Vonk.TestPlugins.TestReadPayload
{
    public class SeekableBodyMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<SeekableBodyMiddleware> _logger;

        public SeekableBodyMiddleware(RequestDelegate next, ILogger<SeekableBodyMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            context.Request.EnableBuffering();
            await _next(context);
        }
    }

    [VonkConfiguration(order: 1000)]
    public static class RewindConfiguration
    {
        public static IApplicationBuilder UseRewind(this IApplicationBuilder app)
        {
            return app.UseMiddleware<SeekableBodyMiddleware>();
        }
    }

    public class DumpPayloadMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<DumpPayloadMiddleware> _logger;

        public DumpPayloadMiddleware(RequestDelegate next, ILogger<DumpPayloadMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            var bodyStream = context.Request.Body;
            if (bodyStream != Stream.Null && bodyStream.CanSeek) //Check CanSeek otherwise we can't reset the stream to it's original position and possibly ruin next middleware.
            {
                var originalPosition = bodyStream.Position;
                // Leave the body open so the next middleware can read it.
                using (var bodyReader = new StreamReader(
                    context.Request.Body,
                    encoding: Encoding.UTF8,
                    detectEncodingFromByteOrderMarks: false,
                    bufferSize: 1024,
                    leaveOpen: true))
                {
                    var resourceString = await bodyReader.ReadToEndAsync();
                    bodyStream.Seek(originalPosition, SeekOrigin.Begin);
                    _logger.LogTrace("Body is: {body}", resourceString);
                }
            }
            await _next(context);
        }
    }

    [VonkConfiguration(order: 1050)]
    public static class DumpPayloadConfiguration
    {
        public static IApplicationBuilder UseDumpPayload(this IApplicationBuilder app)
        {
            return app.UseMiddleware<DumpPayloadMiddleware>();
        }
    }
}
